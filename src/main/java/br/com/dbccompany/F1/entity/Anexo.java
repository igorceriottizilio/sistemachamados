package br.com.dbccompany.F1.entity;

import br.com.dbccompany.F1.dto.AnexoDTO;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Anexo.class)
public class Anexo {

    @Id
    @SequenceGenerator(allocationSize = 1, name ="anexo_seq", sequenceName = "anexo_seq")
    @GeneratedValue(generator = "anexo_seq", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(length = 300, nullable = false)
    private String url;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_chamado")
    private Chamado chamado;

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Chamado getChamado() {
        return chamado;
    }

    public void setChamado(Chamado chamado) {
        this.chamado = chamado;
    }

    public AnexoDTO geraDTO(){
        return new AnexoDTO(this.id, this.url, this.chamado.getId());
    }

}
