package br.com.dbccompany.F1.security;

import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.TipoUsuario;
import br.com.dbccompany.F1.service.UsuarioService;
import br.com.dbccompany.F1.verification.Permissoes;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    UsuarioService usuarioService = null;

    protected JWTLoginFilter(String url, AuthenticationManager authManager, UsuarioService usuarioService) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
        this.usuarioService = usuarioService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        Usuario usuario = new ObjectMapper().readValue(request.getInputStream(), Usuario.class);
        TipoUsuario tipo = usuarioService.buscarPorUsername(usuario.getUsername()).getTipoUsuario();
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        usuario.getUsername(),
                        usuario.getPassword(),
                        Permissoes.definirRoles(tipo)
                )
        );
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain,
            Authentication auth) throws IOException, ServletException {
        TokenAuthenticationService.addAuthentication(response,auth.getName());
    }
}
