package br.com.dbccompany.F1.dto;

public class AnexoDTO {
    private long id;
    private String url;
    private long idChamado;

    public AnexoDTO(long id, String url, long idChamado) {
        this.id = id;
        this.url = url;
        this.idChamado = idChamado;
    }

    // Getters and Setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getIdChamado() {
        return idChamado;
    }

    public void setIdChamado(long idChamado) {
        this.idChamado = idChamado;
    }

}
