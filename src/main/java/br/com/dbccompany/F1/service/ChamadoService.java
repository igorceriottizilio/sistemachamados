package br.com.dbccompany.F1.service;

import br.com.dbccompany.F1.entity.Anexo;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Usuario;
import br.com.dbccompany.F1.enumeration.Status;
import br.com.dbccompany.F1.repository.ChamadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ChamadoService {
    @Autowired
    ChamadoRepository chamadoRepository;

    @Autowired
    UsuarioService usuarioService;

    @Transactional( rollbackFor = Exception.class )
    public Chamado salvar(Chamado chamado ) throws Exception {
        Usuario usuario = usuarioService.buscarPorId(chamado.getUsuario().getId());
        chamado.setUsuario(usuario);
        return chamadoRepository.save( chamado );
    }

    public Chamado buscarPorId(long id ){
        return chamadoRepository.findById(id).get();
    }

    public List<Chamado> buscarPorUsuario(long id){
        Usuario usuario = usuarioService.buscarPorId(id);
        return chamadoRepository.findAllByUsuario(usuario);
    }

    public List<Chamado> listarTodos() {
        return (List<Chamado>) chamadoRepository.findAll();
    }

    public Chamado buscarPorStatus(Status status ){
        return chamadoRepository.findAllByStatus(status);
    }

    @Transactional
    public Chamado buscarPorAnexo(Anexo anexo){
        return chamadoRepository.findByAnexos(anexo);
    }

}
