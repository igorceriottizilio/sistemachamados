package br.com.dbccompany.F1.repository;

import br.com.dbccompany.F1.enumeration.Status;
import br.com.dbccompany.F1.entity.Anexo;
import br.com.dbccompany.F1.entity.Chamado;
import br.com.dbccompany.F1.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChamadoRepository extends CrudRepository<Chamado, Long> {
    Chamado findAllByStatus(Status status);
    List<Chamado> findAllByUsuario(Usuario usuario);
    Chamado findByAnexos(Anexo anexo);
}
